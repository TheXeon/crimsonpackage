Hey there :)

Table of Contents: (Use Control+F to navigate this document, whatitis - whattocontrol+ffor)
1. So Hi! - Alpha
2. Folder name syntax. - Bravo
3. What is actually in this folder? - Charmander

..........ALPHA..........

In this folder, you will find the HUD, working on an updater when I have the time.

This is a copy of my /tf/custom folder, the Folder and .vpks are the HUD and some fixes respectively.
Updates for this HUD and related mods can be found here, check often or when asked by me to update: 
https://www.dropbox.com/sh/6r3yqqpisx50fpn/AAD-QKCxOY4ktLYY0rmmp0T8a?dl=0

..........BRAVO..........

CrimsonHUD folder name syntax:
[CrimsonHUD]: Constant, or until name change.
[V#.#.#]: Version Number.
[YYYY-MM-DD]: Preceded by an underscore, this is the internationally accepted and utilized pattern of stating
			 the date on any document. It is not supposed to be in any other way besides this, at least online.
//
..........CHARMANDER..........

Explanation of WHAT IS IN THIS FOLDER:
You will find the latest CrimsonHUD, along with some mods, customization, and other tweaks. These can, of course, be dragged
straight into the /tf/custom folder.

Folders:

Crimson HUD folder name syntax is explained above, from now on, I will be bundling mods/plugins separately, as well as
explaining how to customize each if the respective explanations for them aren't sufficient.

Customizations: Any little tweaks you guys ask of me, I try to put in here. Any requests will be fulfilled (don't cheat 
though).

VPK's:

LoD Project: Highly recommended, creates LoD meshes to reduce lag on distance. Only about 8% done and its been 2 years.

nohatsmod: Basically in the name, disables hats on all players except for some new ones. Gets updated often.
Blasphemous, I know.

nosoundscapes: Disables ambient sounds on maps, basically taking the dubstep or bird noises off those kinds of maps.

tip_itemfx: Nifty little mod that will attempt to revamp the particles, optimizing and preventing lag spikes that
EVERYONE faces, even if you have a high-end computer.

PREC: A little plugin that automatically records demos, does not break often. Please use regardless of whether you
actually want to or not.

OF ALL VPKS ABOVE, YOU ONLY NEED TO INSTALL THE ONES YOU LIKE/NEED. THEY ARE FOR FPS PURPOSES, BUT ARE STILL WIDELY 
USED IN COMPETITIVE PLAY.


